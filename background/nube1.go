components {
  id: "nube1"
  component: "/background/nube1.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "sprite"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -66.0
    y: -84.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite1"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -279.0
    y: 73.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite2"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 438.0
    y: 196.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite3"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 243.0
    y: -42.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite4"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 46.0
    y: 134.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite5"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 354.0
    y: -237.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite6"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -213.0
    y: 286.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
embedded_components {
  id: "sprite7"
  type: "sprite"
  data: "tile_set: \"/background/background.atlas\"\n"
  "default_animation: \"nube\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -200.0
    y: -247.0
    z: 0.0
  }
  rotation {
    x: 1.4669886E-17
    y: -0.041599084
    z: -1.0310028E-17
    w: 0.99913436
  }
}
